﻿using ExpressionParserCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExpressionWpfDemo
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string expressionText;
        private string resultText;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            PropertyChanged += MainWindow_PropertyChanged;
            ExpressionText = "10+5*(6.2-4)+Round(10.3333)+Sqrt(121)";
        }

        private void MainWindow_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ExpressionText))
            {
                Eval();
            }
        }

        public void Eval()
        {
            try
            {
                var result = ExpressionParser.Parse(expressionText, FunctionResolver.DefaultFunctionResolver);
                var value = ExpressionEvaluator.Eval(result, null, FunctionResolver.DefaultFunctionResolver);
                ResultText = "" + value;
            }
            catch (ExpressionException ex)
            {
                ResultText = ex.Message + $" (Token: '{ex.Token})'";
            }
            catch (Exception e)
            {
                ResultText = e.Message;
            }
        }

        public string ExpressionText
        {
            get => expressionText;
            set
            {
                expressionText = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ExpressionText)));
            }
        }

        public string ResultText
        {
            get => resultText;
            set
            {
                resultText = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ResultText)));
            }
        }

    }
}
