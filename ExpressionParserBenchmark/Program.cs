﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using ExpressionParserCore;
using System;

namespace ExpressionParserBenchmark
{
    public class ExpressionsBenchmark
    {
        [Benchmark]
        public string TryParse() => ExpressionParser.TryParse("42*8+150/85-50+80/90*45+1235/8*32/4", out string value) ? value : null;

        [Benchmark(Baseline = true)]
        public string Parse() => ExpressionParser.Parse("42*8+150/85-50+80/90*45+1235/8*32/4");

        [Benchmark]
        public string ParseError()
        {
            try
            {
                return ExpressionParser.Parse("42*8+150//85-50+80/90*45+1235/8*32/4");
            }
            catch { return null; }
        }

        [Benchmark]
        public string TryParseError() => ExpressionParser.TryParse("42*8+150//85-50+80/90*45+1235/8*32/4", out string value) ? value : null;
    }

    class Program
    {
        static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run<ExpressionsBenchmark>();
            Console.ReadKey();
        }
    }
}
