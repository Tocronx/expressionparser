﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ExpressionParserCore
{
    /// <summary>
    /// Evaluator class for postfix expressions.
    /// </summary>
    public static class ExpressionEvaluator
    {
        /// <summary>
        /// Evaluator for postfix expressions. Support for functions and variables can be added by passing a resolver.
        /// </summary>
        /// <param name="postfixExpression">The postfix expression to evaluate.</param>
        /// <param name="variableResolver">Optional: A variable resolver.</param>
        /// <param name="functionResolver">Optional: A function resolver.</param>
        /// <returns>The result value of the expression evaluation.</returns>
        public static double Eval(string postfixExpression, IVariableResolver variableResolver = null, IFunctionResolver functionResolver = null)
        {
            if (string.IsNullOrWhiteSpace(postfixExpression))
            {
                throw new ExpressionException(ExpressionError.InvalidExpression, "");
            }
            var stack = new Stack<string>(postfixExpression.Split(' '));
            var result = Eval(stack, (functionResolver, variableResolver));
            if (stack.Count > 0)
            {
                throw new ExpressionException(ExpressionError.InvalidExpression, "");
            }
            return result;
        }

        private static double Eval(Stack<string> stack, (IFunctionResolver functionResolver, IVariableResolver variableResolver) data)
        {
            if (stack == null || stack.Count == 0)
            {
                throw new ExpressionException(ExpressionError.InvalidExpression, "");
            }
            double temp;
            var item = stack.Pop();
            switch (item[0])
            {
                //case '=':
                //    temp = Eval(stack, metadata);
                //    if (stack.Count == 0) { throw new ExpressionException(ExpressionError.InvalidExpression, "="); }
                //    variableResolver.DeclareVariable(stack.Pop(), temp);
                //    return temp;
                case '§':
                    return -Eval(stack, data);
                case '+':
                    return Eval(stack, data) + Eval(stack, data);
                case '-':
                    temp = Eval(stack, data);
                    return Eval(stack, data) - temp;
                case '*':
                    return Eval(stack, data) * Eval(stack, data);
                case '/':
                    temp = Eval(stack, data);
                    if (temp == 0) { throw new ExpressionException(ExpressionError.DivisionByZero, "/"); }
                    return Eval(stack, data) / temp;
                case '%':
                    temp = Eval(stack, data);
                    if (temp == 0) { throw new ExpressionException(ExpressionError.DivisionByZero, "%"); }
                    return Eval(stack, data) % temp;
                case '^':
                    temp = Eval(stack, data);
                    return Math.Pow(Eval(stack, data), temp);
                case '!':
                    int count = int.Parse(item.Substring(1, 1));
                    double[] parameter = new double[count];
                    for (int i = count - 1; i >= 0; --i)
                    {
                        parameter[i] = Eval(stack, data);
                    }
                    if (data.functionResolver == null)
                    {
                        throw new ExpressionException(ExpressionError.FunctionsUnsuported, item.Substring(2));
                    }
                    return data.functionResolver.ExecuteFunction(item.Substring(2), parameter);
                default:
                    if (char.IsDigit(item[0]))
                    {
                        if (double.TryParse(item.Replace(',', '.').Replace('_', ','), NumberStyles.Any, CultureInfo.InvariantCulture, out var result))
                        {
                            return result;
                        }
                        throw new ExpressionException(ExpressionError.InvalidNumber, "");
                    }
                    if (data.variableResolver == null)
                    {
                        throw new ExpressionException(ExpressionError.VariablesUnsuported, item.Substring(2));
                    }
                    try
                    {
                        return data.variableResolver.ResolveVariable(item);
                    }
                    catch (ArgumentException)
                    {
                        throw new ExpressionException(ExpressionError.UnknownVariable, item);
                    }
            }
        }

    }
}
