﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ExpressionParserCore
{
    /// <summary>
    /// Parser class for converting infix to postfix expressions.
    /// </summary>
    public static class ExpressionParser
    {
        /// <summary>
        /// Try parsing an expression in infix notation and converting it to postfix notation.
        /// </summary>
        /// <param name="infixExpression">The expression in infix notation.</param>
        /// <param name="postfixExpression">Out parameter for the expression transformed into postfix notation</param>
        /// <param name="functionResolver">Optional: A function resolver</param>
        public static bool TryParse(string infixExpression, out string postfixExpression, IFunctionResolver functionResolver = null)
        {
            return ParseExpression(infixExpression, out postfixExpression, functionResolver, false);
        }

        /// <summary>
        /// Parses an expression in infix notation and converts it to postfix notation.
        /// </summary>
        /// <param name="infixExpression">The expression in infix notation.</param>
        /// <param name="functionResolver">Optional: A function resolver</param>
        /// <returns>The expression transformed into postfix notation</returns>
        public static string Parse(string infixExpression, IFunctionResolver functionResolver = null)
        {
            ParseExpression(infixExpression, out string postfixExpression, functionResolver, true);
            return postfixExpression;
        }

        private static bool ParseExpression(string infixExpression, out string postfixExpression, IFunctionResolver functionResolver, bool useExceptions)
        {
            postfixExpression = null;
            if (string.IsNullOrEmpty(infixExpression))
            {
                if (!useExceptions) { return false; }
                throw new ExpressionException(ExpressionError.NoExpression, "");
            }
            int expectedLength = 1;
            var opSet = new HashSet<char>() { '+', '-', '*', '/', /*'=',*/ '^', '%', ';' };
            var tokens = Tokenize(infixExpression);
            var postfixStack = new Stack<string>();
            var tempStack = new Stack<string>();
            var parameterStack = new Stack<int>();
            (string Item, ExpressionError? Error) result;
            for (int i = 0; i < tokens.Count; ++i)
            {
                var token = tokens[i];
                switch (token[0])
                {
                    case '(':
                        if (i > 0 && !opSet.Contains(tokens[i - 1][0]) && tokens[i - 1][0] != '(' && tokens[i - 1].Length > 0 && !(functionResolver?.IsFunction(tokens[i - 1]) == true))
                        {
                            if (!useExceptions) { return false; }
                            throw new ExpressionException(ExpressionError.OperatorExpected, token);

                        }
                        tempStack.Push(token);
                        break;
                    case ')':
                        if (tempStack.Count == 0)
                        {
                            if (!useExceptions) { return false; }
                            throw new ExpressionException(ExpressionError.OpeningBracketWithoutClosing, token);
                        }
                        while (tempStack.Count != 0)
                        {
                            var item = tempStack.Pop();
                            if (item[0] == '(') { break; }
                            else if (tempStack.Count == 0)
                            {
                                if (!useExceptions) { return false; }
                                throw new ExpressionException(ExpressionError.OpeningBracketWithoutClosing, token);
                            }
                            result = MethodCheck(item);
                            if (result.Error != null)
                            {
                                if (!useExceptions) { return false; }
                                throw new ExpressionException((ExpressionError)result.Error, result.Item);
                            }
                            postfixStack.Push(result.Item);
                        }
                        break;
                    case '-':
                    case '+':
                    case '*':
                    case '/':
                    case '%':
                    //case '=':
                    case '^':
                        expectedLength++;
                        if (token[0] == '-' && (i == 0 || opSet.Contains(tokens[i - 1][0]) || tokens[i - 1][0] == '('))
                        {
                            if (i < tokens.Count - 1 && (tokens[i + 1][0] == ')' || opSet.Contains(tokens[i + 1][0])))
                            {
                                if (!useExceptions) { return false; }
                                throw new ExpressionException(ExpressionError.InvalidChar, tokens[i + 1]);
                            }
                            token = "§";
                            tempStack.Push(token);
                            break;
                        }
                        expectedLength++;
                        if (i > 0 && opSet.Contains(tokens[i - 1][0]) || i == 0 || i == tokens.Count - 1)
                        {
                            if (!useExceptions) { return false; }
                            throw new ExpressionException(ExpressionError.InvalidChar, tokens[i]);
                        }
                        while (tempStack.Count != 0)
                        {
                            var item = tempStack.Peek();
                            if (item.Length == 1 && (Prec(token[0]) > Prec(item[0])
                                || (token[0] == '^' && item[0] == '^')
                                || (token[0] == '§' && item[0] == '§'))) { break; }
                            item = tempStack.Pop();
                            result = MethodCheck(item);
                            if (result.Error != null)
                            {
                                if (!useExceptions) { return false; }
                                throw new ExpressionException((ExpressionError)result.Error, result.Item);
                            }
                            postfixStack.Push(result.Item);
                        }
                        tempStack.Push(token);
                        break;
                    case ';':
                        expectedLength++;
                        if (parameterStack.Count > 0)
                        {
                            if (i < tokens.Count - 1 && (tokens[i + 1][0] == ')' || tokens[i + 1][0] == ';'))
                            {
                                if (!useExceptions) { return false; }
                                throw new ExpressionException(ExpressionError.InvalidChar, tokens[i + 1]);
                            }
                            parameterStack.Push(parameterStack.Pop() + 1);
                            while (tempStack.Count != 0)
                            {
                                var item = tempStack.Pop();
                                if (item[0] == '(') { tempStack.Push(item); break; }
                                result = MethodCheck(item);
                                if (result.Error != null)
                                {
                                    if (!useExceptions) { return false; }
                                    throw new ExpressionException((ExpressionError)result.Error, result.Item);
                                }
                                postfixStack.Push(result.Item);
                            }
                        }
                        else
                        {
                            if (!useExceptions) { return false; }
                            throw new ExpressionException(ExpressionError.InvalidChar, token);
                        }
                        break;
                    default:
                        if (i > 0 && (!opSet.Contains(tokens[i - 1][0]) && tokens[i - 1][0] != '('))
                        {
                            if (!useExceptions) { return false; }
                            throw new ExpressionException(ExpressionError.OperatorExpected, token);
                        }
                        if (char.IsDigit(token[0]))
                        {
                            if (double.TryParse(token.Replace(',', '.').Replace('_', ','), NumberStyles.Any, CultureInfo.InvariantCulture, out var value))
                            {
                                postfixStack.Push(token);
                                break;
                            }
                            if (!useExceptions) { return false; }
                            throw new ExpressionException(ExpressionError.InvalidNumber, token);
                        }
                        else if (char.IsLetter(token[0]) || token[0] == '#' || token[0] == '$')
                        {
                            if (i < tokens.Count - 1 && tokens[i + 1][0] == '(')
                            {
                                if (functionResolver?.IsFunction(token) == true)
                                {
                                    expectedLength++;
                                    tempStack.Push(token);
                                    parameterStack.Push(1);
                                    break;
                                }
                                if (!useExceptions) { return false; }
                                throw new ExpressionException(ExpressionError.UnknownMethod, token);
                            }
                            postfixStack.Push(token);
                            break;
                        }
                        if (!useExceptions) { return false; }
                        throw new ExpressionException(ExpressionError.InvalidChar, token);
                }
            }
            while (tempStack.Count != 0)
            {
                var item = tempStack.Pop();
                if (item[0] == '(')
                {
                    if (!useExceptions) { return false; }
                    throw new ExpressionException(ExpressionError.ClosingBracketWithoutOpening, item);
                }
                result = MethodCheck(item);
                if (result.Error != null)
                {
                    if (!useExceptions) { return false; }
                    throw new ExpressionException((ExpressionError)result.Error, result.Item);
                }
                postfixStack.Push(result.Item);
            }
            if (postfixStack.Count == 0)
            {
                if (!useExceptions) { return false; }
                throw new ExpressionException(ExpressionError.NoExpression, "");
            }
            if (expectedLength != postfixStack.Count)
            {
                if (!useExceptions) { return false; }
                throw new ExpressionException(ExpressionError.InvalidExpression, "");
            }
            postfixExpression = string.Join(" ", postfixStack.ToArray().Reverse());
            return true;
            (string Item, ExpressionError? Error) MethodCheck(string item)
            {
                if (item.Length > 1)
                {
                    int parameters = parameterStack.Pop();
                    if (functionResolver.IsFunction(item, parameters))
                    {
                        return ("!" + parameters + item, null);
                    }
                    return (item, ExpressionError.InvalidNumberOfParameters);
                }
                return (item, null);
            }
        }

        private static int Prec(char c)
        {
            switch (c)
            {
                //case '=':
                //    return 1;
                case '+':
                case '-':
                    return 2;
                case '*':
                case '/':
                case '%':
                    return 3;
                case '§':
                    return 4;
                case '^':
                    return 5;
            }
            return -1;
        }

        private static List<string> Tokenize(string expression)
        {
            var tokens = new List<string>();
            var span = expression.AsSpan();
            int comment = 0, pos = 0, i = 0;
            for (; i != span.Length; ++i)
            {
                switch (span[i])
                {
                    case '\n':
                    case '\r':
                        if (comment > 0)
                        {
                            if (comment == 1) { comment = 0; pos = -1; }
                            break;
                        }
                        Read(span);
                        break;
                    case '/':
                        if (comment > 0)
                        {
                            if (i > 0 && span[i - 1] == '*') { comment = 0; pos = -1; }
                            break;
                        }
                        else if (i < span.Length - 1)
                        {
                            if (span[i + 1] == '/') { comment = 1; pos = i; break; }
                            if (span[i + 1] == '*') { comment = 2; pos = i; break; }
                        }
                        Read(span);
                        tokens.Add("/");
                        break;
                    case '-':
                    case '+':
                    case '*':
                    case '%':
                    case '^':
                    //case '=':
                    case ';':
                        if (comment > 0) { break; }
                        Read(span);
                        tokens.Add(span[i].ToString());
                        break;
                    case '[':
                    case '(':
                    case '{':
                        if (comment > 0) { break; }
                        Read(span);
                        tokens.Add("(");
                        break;
                    case ']':
                    case ')':
                    case '}':
                        if (comment > 0) { break; }
                        Read(span);
                        tokens.Add(")");
                        break;
                    case '²':
                        if (comment > 0) { break; }
                        Read(span);
                        tokens.Add("^");
                        tokens.Add("2");
                        break;
                    case '³':
                        if (comment > 0) { break; }
                        Read(span);
                        tokens.Add("^");
                        tokens.Add("3");
                        break;
                    case ' ':
                    case '\t':
                        if (comment > 0) { break; }
                        Read(span);
                        break;
                    default:
                        if (pos == -1) { pos = i; }
                        break;
                }
            }
            if (comment == 0) { Read(span); }
            return tokens;
            void Read(ReadOnlySpan<char> s)
            {
                if (pos != -1 && i > pos)
                {
                    tokens.Add(new string(s.Slice(pos, i - pos).ToArray()));
                }
                pos = -1;
            }
        }

    }
}