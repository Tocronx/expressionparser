﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExpressionParserCore
{
    /// <summary>
    /// Example implementation for a simple function resolver.
    /// </summary>
    public class FunctionResolver : IFunctionResolver
    {
        public static readonly FunctionResolver DefaultFunctionResolver = new FunctionResolver();

        protected readonly Dictionary<(string Name, int Parameters), Func<double[], double>> functions =
            new Dictionary<(string Name, int Parameters), Func<double[], double>>();
        protected readonly HashSet<string> functionNames = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        public FunctionResolver()
        {
            AddFunction(nameof(Math.Abs), x => Math.Abs(x[0]), 1);
            AddFunction(nameof(Math.Sqrt), x => Math.Sqrt(x[0]), 1);
            AddFunction(nameof(Math.Round), x => Math.Round(x[0], MidpointRounding.AwayFromZero), 1);
            AddFunction(nameof(Math.Round), x => Math.Round(x[0] * Math.Pow(10, (int)x[1]), 0, MidpointRounding.AwayFromZero) / Math.Pow(10, (int)x[1]), 2);
            AddFunction(nameof(Math.Pow), x => Math.Pow(x[0], x[1]), 2);
            AddFunction(nameof(Math.Max), x => Math.Max(x[0], x[1]), 2);
            AddFunction(nameof(Math.Min), x => Math.Min(x[0], x[1]), 2);
            AddFunction(nameof(Math.Log), x => Math.Log(x[0]), 1);
            AddFunction(nameof(Math.Log), x => Math.Log(x[0], x[1]), 2);
            AddFunction(nameof(Math.Log10), x => Math.Log10(x[0]), 1);
            AddFunction(nameof(Math.Floor), x => Math.Floor(x[0]), 1);
            AddFunction(nameof(Math.Ceiling), x => Math.Ceiling(x[0]), 1);
            AddFunction(nameof(Math.Exp), x => Math.Exp(x[0]), 1);
            AddFunction(nameof(Math.Atan2), x => Math.Atan2(x[0], x[1]), 2);
            AddFunction(nameof(Math.Sin), x => Math.Sin(x[0]), 1);
            AddFunction(nameof(Math.Asin), x => Math.Asin(x[0]), 1);
            AddFunction(nameof(Math.Sinh), x => Math.Sinh(x[0]), 1);
            AddFunction(nameof(Math.Cos), x => Math.Cos(x[0]), 1);
            AddFunction(nameof(Math.Acos), x => Math.Acos(x[0]), 1);
            AddFunction(nameof(Math.Cosh), x => Math.Cosh(x[0]), 1);
            AddFunction(nameof(Math.Tan), x => Math.Tan(x[0]), 1);
            AddFunction(nameof(Math.Atan), x => Math.Atan(x[0]), 1);
            AddFunction(nameof(Math.Tanh), x => Math.Tanh(x[0]), 1);
        }

        public double ExecuteFunction(string name, params double[] parameters)
        {
            if (functions.TryGetValue((name?.ToLower(), parameters.Length), out var function))
            {
                return function(parameters);
            }
            throw new ArgumentException($"No function '{name}' with {parameters.Length} paremters found!");
        }

        public bool IsFunction(string name, int? numberOfParameters = null)
        {
            if (string.IsNullOrWhiteSpace(name)) { return false; }
            if (numberOfParameters == null) { return functionNames.Contains(name); }
            return functions.ContainsKey((name.ToLower(), (int)numberOfParameters));
        }

        public List<(string Name, int NumberOfParameters)> GetFunctionList() => functions.Keys.ToList();

        private void AddFunction(string name, Func<double[], double> function, int numberOfParameters = 0)
        {
            if (string.IsNullOrWhiteSpace(name) || numberOfParameters < 0)
            {
                throw new ArgumentException($"Functions must have a name and >= 0 paremters!");
            }
            functions.Add((name.ToLower(), numberOfParameters), function);
            functionNames.Add(name);
        }

    }
}
