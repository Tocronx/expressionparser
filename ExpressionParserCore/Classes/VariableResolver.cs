﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExpressionParserCore
{
    /// <summary>
    /// Example implementation for a simple variable resolver.
    /// </summary>
    public class VariableResolver : IMutableVariableResolver
    {
        protected readonly Dictionary<string, double> variables = new Dictionary<string, double>(StringComparer.OrdinalIgnoreCase);

        public IVariableResolver ParentVariableResolver { get; set; }

        public VariableResolver(bool declareDefaults = false)
        {
            if (declareDefaults)
            {
                variables.Add("PI", Math.PI);
                variables.Add("E", Math.E);
            }
        }

        public double ResolveVariable(string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                if (variables.TryGetValue(name, out var value)) { return value; }
                var temp = ParentVariableResolver;
                if (temp != null) { return temp.ResolveVariable(name); }
            }
            throw new ArgumentException($"Variable '{name}' must be defined first!", nameof(name));
        }

        public bool IsVariable(string name) => !string.IsNullOrWhiteSpace(name) && variables.ContainsKey(name);

        public List<string> GetVariableList() => variables.Keys.ToList();

        public void DeclareVariable(string name, double value) => variables[name] = value;

        public void RemoveVariable(string name) => variables.Remove(name);
    }
}
