﻿namespace ExpressionParserCore
{
    /// <summary>
    /// Enum for the different types of errors that can occur when parsing expressions.
    /// </summary>
    public enum ExpressionError
    {
        NoExpression,
        OpeningBracketWithoutClosing,
        ClosingBracketWithoutOpening,
        UnknownMethod,
        UnknownVariable,
        OperatorExpected,
        InvalidNumberOfParameters,
        InvalidNumber,
        InvalidChar,
        DivisionByZero,
        InvalidExpression,
        VariableAlreadyDefined,
        VariablesUnsuported,
        FunctionsUnsuported,
    }
}
