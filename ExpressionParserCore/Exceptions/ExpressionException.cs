﻿using System;

namespace ExpressionParserCore
{
    /// <summary>
    /// The <c>ExpressionException</c> is a specific exception for errors during parsing of expressions.
    /// </summary>
    public class ExpressionException : Exception
    {
        public ExpressionException(ExpressionError errorType, string token) : base(Enum.GetName(typeof(ExpressionError), errorType))
        {
            ErrorType = errorType;
            Token = token;
        }
        public ExpressionError ErrorType { get; private set; }
        public string Token { get; private set; }
    }
}
