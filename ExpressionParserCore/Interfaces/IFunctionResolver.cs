﻿using System.Collections.Generic;

namespace ExpressionParserCore
{
    /// <summary>
    /// Interface for the base functionalities for resolving function calls.
    /// </summary>
    public interface IFunctionResolver
    {
        /// <summary>
        /// Executes the function with the specified name and parameters and returns the result.
        /// </summary>
        /// <param name="name">Name of the function to execute.</param>
        /// <param name="parameters">The parameters for the function call.</param>
        /// <returns>The result value of the function call.</returns>
        double ExecuteFunction(string name, params double[] parameters);

        /// <summary>
        /// Checks if the function with the specified name and parameter count is supported by the function resolver.
        /// </summary>
        /// <param name="name">Name of the function.</param>
        /// <param name="numberOfParameters">Optional: Number of parameters that the function must have.</param>
        /// <returns>True if such a function exists otherwise false.</returns>
        bool IsFunction(string name, int? numberOfParameters = null);

        /// <summary>
        /// Gets the list of all functions supported by the function resolver.
        /// </summary>
        /// <returns>List of all functions as list of tuples from function names and parameter count.</returns>
        List<(string Name, int NumberOfParameters)> GetFunctionList();
    }
}
