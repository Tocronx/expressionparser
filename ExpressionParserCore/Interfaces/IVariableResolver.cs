﻿using System.Collections.Generic;

namespace ExpressionParserCore
{
    /// <summary>
    /// Interface for the base functionalities for resolving variables.
    /// </summary>
    public interface IVariableResolver
    {
        /// <summary>
        /// Resolves the variable with specified name and returns the value.
        /// </summary>
        /// <param name="name">Name of the variable.</param>
        /// <returns>The value of variable.</returns>
        double ResolveVariable(string name);

        /// <summary>
        /// Checks if the variable with the specified name is existing.
        /// </summary>
        /// <param name="name">Name of the variable.</param>
        /// <returns>True if such a variable exists otherwise false.</returns>
        bool IsVariable(string name);

        /// <summary>
        /// Gets the list of all variables supported by the variable resolver.
        /// </summary>
        /// <returns>List of all variable names.</returns>
        List<string> GetVariableList();
    }

    /// <summary>
    /// Interface for the extended functionalities like add and remove variable declarations.
    /// </summary>
    public interface IMutableVariableResolver : IVariableResolver
    {
        /// <summary>
        /// Declares a variable with the given name and value.
        /// </summary>
        /// <param name="name">The name of the variable.</param>
        /// <param name="value">The value of the variable.</param>
        void DeclareVariable(string name, double value);

        /// <summary>
        /// Removes the variable with the given name.
        /// </summary>
        /// <param name="name">The name of the variable.</param>
        void RemoveVariable(string name);
    }
}
