# ExpressionParser

Simple expression/formula parser. Allows the conversion of formulas in infix notation to postfix notation. Calculates formulas in postfix notation using resolver classes for variables and functions.