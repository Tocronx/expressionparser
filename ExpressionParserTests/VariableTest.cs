using ExpressionParserCore;
using System;
using Xunit;

namespace ExpressionParserTests
{
    public class VariableTest
    {
        [Theory]
        [InlineData("pi", Math.PI)]
        [InlineData("Pi", Math.PI)]
        [InlineData("pI", Math.PI)]
        [InlineData("PI", Math.PI)]
        [InlineData("-PI", -Math.PI)]
        [InlineData("PI+1", Math.PI + 1)]
        [InlineData("1+PI", 1 + Math.PI)]
        [InlineData("5*PI", 5 * Math.PI)]
        public void ValidVariableTest(string expression, double result)
        {
            var functionResolver = new FunctionResolver();
            var variableResolver = new VariableResolver(true);
            var value = ExpressionEvaluator.Eval(ExpressionParser.Parse(expression), variableResolver);
            Assert.Equal(value, result);
        }

        [Theory]
        [InlineData("test")]
        [InlineData("q")]
        [InlineData("q+5")]
        [InlineData("5+q")]
        public void InvalidVariableTest(string expression)
        {
            var functionResolver = new FunctionResolver();
            var variableResolver = new VariableResolver(true);
            Assert.Throws<ExpressionException>(() => ExpressionEvaluator.Eval(ExpressionParser.Parse(expression), variableResolver));
        }

    }
}
