using ExpressionParserCore;
using System;
using Xunit;

namespace ExpressionParserTests
{
    public class BracketsTest
    {
        [Theory]
        [InlineData("(3)", 3)]
        [InlineData("(3+3)", 6)]
        [InlineData("3-(-3)", 6)]
        [InlineData("3*(2+1)", 9)]
        [InlineData("(3*0,5)", 1.5)]
        [InlineData("3/(1+2)", 1)]
        [InlineData("((1+2)*((4/2)+1))", 9)]
        [InlineData("-(5)", -5)]
        [InlineData("8/2*(2+2)", 16)]
        public void ValidBracketsTest(string expression, double result)
        {
            var value = ExpressionEvaluator.Eval(ExpressionParser.Parse(expression));
            Assert.Equal(value, result);
        }

        [Theory]
        [InlineData(")")]
        [InlineData("(")]
        [InlineData(")(")]
        [InlineData("()(")]
        [InlineData(")()")]
        [InlineData("(()")]
        [InlineData("())")]
        [InlineData("(5+)")]
        [InlineData("(5-)")]
        [InlineData("(5*)")]
        [InlineData("(5/)")]
        [InlineData("(5%)")]
        [InlineData("(5^/)")]
        [InlineData("+()")]
        [InlineData("-()")]
        [InlineData("*()")]
        [InlineData("/()")]
        [InlineData("%()")]
        [InlineData("^()")]
        public void InvalidBracketsTest(string expression)
        {
            Assert.Throws<ExpressionException>(() => ExpressionEvaluator.Eval(ExpressionParser.Parse(expression)));
        }

    }
}
