using ExpressionParserCore;
using System;
using Xunit;

namespace ExpressionParserTests
{
    public class ArithmeticTest
    {
        [Theory]
        [InlineData("3", 3)]
        [InlineData("3+3", 6)]
        [InlineData("3-3", 0)]
        [InlineData("3*3", 9)]
        [InlineData("3*0,5", 1.5)]
        [InlineData("3/3", 1)]
        [InlineData("3/2", 1.5)]
        public void ElementaryArithmeticTheory(string expression, double result)
        {
            var value = ExpressionEvaluator.Eval(ExpressionParser.Parse(expression));
            Assert.Equal(value, result);
        }

        [Theory]
        [InlineData("3*3+3", 12)]
        [InlineData("3+3*3", 12)]
        [InlineData("3*3-3", 6)]
        [InlineData("3-3*3", -6)]
        [InlineData("3/3+3", 4)]
        [InlineData("3+3/3", 4)]
        [InlineData("3/3-3", -2)]
        [InlineData("3-3/3", 2)]
        public void ElementaryArithmeticOrderTheory(string expression, double result)
        {
            var value = ExpressionEvaluator.Eval(ExpressionParser.Parse(expression));
            Assert.Equal(value, result);
        }

        [Theory]
        [InlineData("-3", -3)]
        [InlineData("3+-3", 0)]
        [InlineData("3--3", 6)]
        [InlineData("-3*3", -9)]
        [InlineData("3*-3", -9)]
        [InlineData("-3/3", -1)]
        [InlineData("3/-3", -1)]
        public void UnaryMinusSignTheory(string expression, double result)
        {
            var value = ExpressionEvaluator.Eval(ExpressionParser.Parse(expression));
            Assert.Equal(value, result);
        }

        [Theory]
        [InlineData("9%3", 0)]
        [InlineData("10%3", 1)]
        [InlineData("-10%3", -1)]
        [InlineData("10%-3", 1)]
        [InlineData("9%3+1", 1)]
        [InlineData("10%3+1", 2)]
        [InlineData("1+9%3", 1)]
        [InlineData("1+10%3", 2)]
        public void ModuloOperationTheory(string expression, double result)
        {
            var value = ExpressionEvaluator.Eval(ExpressionParser.Parse(expression));
            Assert.Equal(value, result);
        }

        [Theory]
        [InlineData("10%3+2", 3)]
        [InlineData("2+10%3", 3)]
        [InlineData("10%3*2", 2)]
        [InlineData("2*10%3", 2)]
        public void ModuloOperationOrderTheory(string expression, double result)
        {
            var value = ExpressionEvaluator.Eval(ExpressionParser.Parse(expression));
            Assert.Equal(value, result);
        }

        [Theory]
        [InlineData("3/0")]
        [InlineData("3%0")]
        [InlineData("3/0,0")]
        [InlineData("3%0,0")]
        public void DivisionByZeroTheory(string expression)
        {
            Assert.Throws<ExpressionException>(() => ExpressionEvaluator.Eval(ExpressionParser.Parse(expression)));
        }

        [Theory]
        [InlineData("3^1", 3)]
        [InlineData("3^3", 27)]
        [InlineData("3�", 9)]
        [InlineData("3�", 27)]
        [InlineData("-3^3", -27)]
        [InlineData("4^-1", 0.25)]
        [InlineData("9^0,5", 3)]
        public void ExponentiationTheory(string expression, double result)
        {
            var value = ExpressionEvaluator.Eval(ExpressionParser.Parse(expression));
            Assert.Equal(value, result);
        }

        [Theory]
        [InlineData("2+2^2", 6)]
        [InlineData("2^2+2", 6)]
        [InlineData("2*2^2", 8)]
        [InlineData("2^2*2", 8)]
        public void ExponentiationOrderTheory(string expression, double result)
        {
            var value = ExpressionEvaluator.Eval(ExpressionParser.Parse(expression));
            Assert.Equal(value, result);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("+")]
        [InlineData("-")]
        [InlineData("*")]
        [InlineData("/")]
        [InlineData("^")]
        [InlineData("%")]
        [InlineData(",")]
        [InlineData(".")]
        [InlineData("�")]
        [InlineData("�")]
        [InlineData("--1")]
        [InlineData("+1")]
        [InlineData("*1")]
        [InlineData("/1")]
        [InlineData("^1")]
        [InlineData("%1")]
        [InlineData("xyz")]
        public void InvalidExpressionsTheory(string expression)
        {
            Assert.Throws<ExpressionException>(() => ExpressionEvaluator.Eval(ExpressionParser.Parse(expression)));
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("+")]
        [InlineData("-")]
        [InlineData("*")]
        [InlineData("/")]
        [InlineData("^")]
        [InlineData("%")]
        [InlineData(",")]
        [InlineData(".")]
        [InlineData("�")]
        [InlineData("�")]
        [InlineData("--1")]
        [InlineData("+1")]
        [InlineData("*1")]
        [InlineData("/1")]
        [InlineData("^1")]
        [InlineData("%1")]
        public void InvalidExpressionsTryTheory(string expression)
        {
            Assert.False(ExpressionParser.TryParse(expression, out string postfixExpression));
        }
    }
}
