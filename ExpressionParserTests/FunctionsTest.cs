using ExpressionParserCore;
using System;
using Xunit;

namespace ExpressionParserTests
{
    public class FunctionsTest
    {
        [Theory]
        [InlineData("Round(1,5)", 2)]
        [InlineData("Round(1,25)", 1)]
        [InlineData("Round(-1,5)", -2)]
        [InlineData("Round(-1,25)", -1)]
        [InlineData("Round(123,123;2)", 123.12)]
        [InlineData("Round(123,123;-2)", 100)]
        public void RoundTheory(string expression, double result)
        {
            var functionResolver = new FunctionResolver();
            var value = ExpressionEvaluator.Eval(ExpressionParser.Parse(expression, functionResolver), null, functionResolver);
            Assert.Equal(value, result);
        }

        [Theory]
        [InlineData("Min(2;4)", 2)]
        [InlineData("Min(4;2)", 2)]
        [InlineData("Min(2;-4)", -4)]
        [InlineData("Max(2;4)", 4)]
        [InlineData("Max(4;2)", 4)]
        [InlineData("Max(2;-4)", 2)]
        public void MinMaxTheory(string expression, double result)
        {
            var functionResolver = new FunctionResolver();
            var value = ExpressionEvaluator.Eval(ExpressionParser.Parse(expression, functionResolver), null, functionResolver);
            Assert.Equal(value, result);
        }

        [Theory]
        [InlineData("Abs(2)", 2)]
        [InlineData("Abs(-2)", 2)]
        [InlineData("Abs(0,2)", 0.2)]
        [InlineData("Abs(-0,2)", 0.2)]
        [InlineData("Abs(-2+5)", 3)]
        [InlineData("Abs(2-5)", 3)]
        public void AbsTheory(string expression, double result)
        {
            var functionResolver = new FunctionResolver();
            var value = ExpressionEvaluator.Eval(ExpressionParser.Parse(expression, functionResolver), null, functionResolver);
            Assert.Equal(value, result);
        }
    }
}
